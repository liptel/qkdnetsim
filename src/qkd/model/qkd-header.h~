/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>, Oliver Mauhart <oliver.maurhart@ait.ac.at>
 *
 * This class represents a single Q3P Message
 * 
 * This is a buffer (== the data sent/received) plus some message stuff
 * 
 * It includes the total package from head to toe. This means:
 * 
 * It includes the header, the payload and the tag. In this order.
 * 
 * This the Q3P message layout:
 * 
 * 
 *      0       4       8               16              24              32
 *      0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0
 *   0  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                            Length                             |
 *   4  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                            Msg-Id                             |
 *   8  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |E A Z r r| Vers|    Protocol   |          Channel              |
 *  12  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                       Encryption Key Id                       |
 *  16  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                     Authentication Key Id                     |
 *  20  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                             A-Tag ...                         
 *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *                              ... A-Tag                               |
 *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      |                             Data ...                           
 *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *                                ... Data ...                          
 *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *                                ... Data                              |
 *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * 
 * with:
 * 
 *      Length:         total size of packet, including the length field itself
 *      Msg-Id:         message number (inside a channel)
 *      E:              Encryption bit. If set the payload is encrypted
 *      A:              Authentication bit. If set the payload is authenticated
 *      Z:              Compressed bit. If set the payload is compressed
 *      r:              reserved for future use
 *      Vers:           Q3P version field: ALWAYS 2 for this implementation
 *      Protocol:       ID of next protocol header in the encrypted header chain
 *      Channel:        Q3P Channel number
 *      Protocols:      List of protocol headers included in carried data (in order)
 *      E-KeyId:        start offset for the encryption key within the buffers
 *      A-KeyId:        start offset for the authentication key within the buffers
 *      Data:           User Data
 *      A-Tag:          Authentication tag
 */
    
 /* Moved to QKDCommandHeader:
 *      Command:        Protocol Command (HANDSHAKE, DATA, LOAD, LOAD-REQUEST, STORE, ...)
 */    

#ifndef QKD_HEADER_H
#define QKD_HEADER_H

#include <queue>
#include <string>
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/object.h"

namespace ns3 {
// define this class in a public header

class QKDCommandHeader : public Header
{
    public:

        QKDCommandHeader ();

        static      TypeId GetTypeId ();
        TypeId      GetInstanceTypeId () const;

        void        Print (std::ostream &os) const;
        bool        operator== (QKDCommandHeader const & o) const;
        uint32_t    GetSerializedSize () const;
        void        Serialize (Buffer::Iterator start) const;
        uint32_t    Deserialize (Buffer::Iterator start);

        void 		SetCommand (char value);
        char 	    GetCommand (void) const;

        void 		SetTransportProtocolNumber (uint8_t value);
        uint8_t 	GetTransportProtocolNumber (void) const;

    private:

        char        m_command;                  /**< command and protocol field used in AIT R10 software. not used in QKD simulation module! */
        uint8_t     m_transportProtocolNumber;
};



class QKDHeader : public Header
{
    public:

        QKDHeader ();

        static TypeId GetTypeId ();
        TypeId GetInstanceTypeId () const;

        void Print (std::ostream &os) const;
        bool operator== (QKDHeader const & o) const;

        uint32_t GetSerializedSize () const;

        void Serialize (Buffer::Iterator start) const;
        uint32_t Deserialize (Buffer::Iterator start);

        void 		SetLength (uint32_t value);
        uint32_t 	GetLength (void) const;

        void 		SetOffset (uint32_t value);
        uint32_t 	GetOffset (void) const;

        void 		SetMessageId (uint32_t value);
        uint32_t 	GetMessageId (void) const;

        void 		SetEncrypted (uint32_t value);
        uint32_t  	GetEncrypted (void) const;

        void 		SetAuthenticated (uint32_t value);
        uint32_t 	GetAuthenticated (void) const;

        void 		SetZipped (uint8_t value);
        uint8_t 	GetZipped (void) const;
     
        void 		SetVersion (char value);
        char 	    GetVersion (void) const;
 
        void 		SetProtocol (uint32_t value);
        uint32_t    GetProtocol (void) const;
      
        void 		SetChannelId (uint16_t value);
        uint16_t 	GetChannelId (void) const;
      
        void 		SetEncryptionKeyId (uint32_t value);
        uint32_t 	GetEncryptionKeyId (void) const;

        void 		SetAuthenticationKeyId (uint32_t value);
        uint32_t 	GetAuthenticationKeyId (void) const;

        void 		SetAuthTag (std::string value);
        std::string GetAuthTag (void) const;

        /// Check that type if valid
        bool IsValid () const
        {
        return m_valid;
        }
         
    private:

        uint32_t        m_length;                   /**< message length field */
        uint32_t        m_offset;                   /**< message offset field used with fragmentation */
        uint32_t        m_messageId;                /**< message id field */

        uint8_t         m_encryped;                 /**< is packet encrypted or not */
        uint8_t         m_authenticated;            /**< is packet authenticated or not */

        uint8_t         m_fragmented;               /**< is packet fragmented or not */
        uint8_t         m_zipped;                   /**< is payload zipped or not */
        uint8_t         m_reserved1;                /**< reserved field for further use */
        uint8_t         m_reserved2;                /**< reserved field for further use */
        char            m_version;                  /**< flags and version field */
        uint32_t        m_protocol;                 /**< identifies next header of the encrypted packet */
        uint16_t        m_channelId;                /**< QKD channel id */  

        uint32_t        m_encryptionKeyId;          /**< encryption key id */
        uint32_t        m_authenticationKeyId;      /**< authentication key id */ 
        std::string     m_authTag;                  /**< authentication tag of the packet */
        bool m_valid;

    };


}  
// namespace ns3

#endif /* QKD_HEADER_H */


