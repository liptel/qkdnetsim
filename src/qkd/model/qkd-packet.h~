/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem@mehic.info>
 */
#ifndef PACKET_H
#define PACKET_H

#include <stdint.h>
#include "buffer.h"
#include "header.h"
#include "trailer.h"
#include "packet-metadata.h"
#include "tag.h"
#include "byte-tag-list.h"
#include "packet-tag-list.h"
#include "nix-vector.h"
#include "ns3/callback.h"
#include "ns3/assert.h"
#include "ns3/ptr.h"
#include "ns3/deprecated.h"

namespace ns3 {

/**
 * \ingroup network
 * \defgroup packet Packet
 */
 
/**
 * \ingroup packet
 * \brief network packets
 *
 * Each network packet contains a byte buffer, a set of byte tags, a set of
 * packet tags, and metadata.
 *
 * - The byte buffer stores the serialized content of the headers and trailers 
 * added to a packet. The serialized representation of these headers is expected
 * to match that of real network packets bit for bit (although nothing
 * forces you to do this) which means that the content of a packet buffer
 * is expected to be that of a real packet.
 *
 * - The metadata describes the type of the headers and trailers which
 * were serialized in the byte buffer. The maintenance of metadata is
 * optional and disabled by default. To enable it, you must call
 * Packet::EnablePrinting and this will allow you to get non-empty
 * output from Packet::Print. If you wish to only enable
 * checking of metadata, and do not need any printing capability, you can
 * call Packet::EnableChecking: its runtime cost is lower than
 * Packet::EnablePrinting.
 *
 * - The set of tags contain simulation-specific information which cannot
 * be stored in the packet byte buffer because the protocol headers or trailers
 * have no standard-conformant field for this information. So-called
 * 'byte' tags are used to tag a subset of the bytes in the packet byte buffer
 * while 'packet' tags are used to tag the packet itself. The main difference
 * between these two kinds of tags is what happens when packets are copied,
 * fragmented, and reassembled: 'byte' tags follow bytes while 'packet' tags
 * follow packets. Another important difference between these two kinds of tags
 * is that byte tags cannot be removed and are expected to be written once,
 * and read many times, while packet tags are expected to be written once,
 * read many times, and removed exactly once. An example of a 'byte' 
 * tag is a FlowIdTag which contains a flow id and is set by the application
 * generating traffic. An example of a 'packet' tag is a cross-layer 
 * qos class id set by an application and processed by a lower-level MAC 
 * layer.
 *
 * Implementing a new type of Header or Trailer for a new protocol is 
 * pretty easy and is a matter of creating a subclass of the ns3::Header 
 * or of the ns3::Trailer base class, and implementing the methods
 * described in their respective API documentation.
 *
 * Implementing a new type of Tag requires roughly the same amount of
 * work and this work is described in the ns3::Tag API documentation.
 *
 * The performance aspects copy-on-write semantics of the
 * Packet API are discussed in \ref packetperf
 */
class QKDPacket : public SimpleRefCount<Packet>
{
public:

    /**
    * \brief Create an empty packet with a new uid (as returned
    * by getUid).
    */
    QKDPacket (uint32_t packetSize);

    Ptr<QKDPacket> Copy (void) const;

    uint32_t GetSize(void) const;
    uint64_t GetUid(void) const;

private:

    static uint64_t     m_globalUid;
    uint32_t            m_size;    

} // namespace ns3

#endif /* PACKET_QKD_H */
