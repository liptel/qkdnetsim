/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#ifndef QKD_GRAPH_H
#define QKD_GRAPH_H

#include <fstream>
#include "ns3/object.h"
#include "ns3/gnuplot.h"
#include "ns3/core-module.h"
#include "ns3/node-list.h"
#include "ns3/qkd-manager.h" 
#include <sstream>

namespace ns3 {

class QKDGraph : public Object 
{
public:
    
    /**
    * \brief Get the type ID.
    * \return the object TypeId
    */
    static TypeId GetTypeId (void);

	QKDGraph (
		uint32_t nodeID,
		uint32_t bufferID,
		std::string graphName,
		std::string graphType
	);

	virtual ~QKDGraph(); 

	void PrintGraph(); 

	void ProcessMCurrent(uint32_t value);

	void ProcessMStatus(uint32_t value); 

	void ProcessMThrStatus(uint32_t value); 

	void ProcessMStatusHelpFunction(double time, uint32_t newValue);

    void InitTotalGraph() const;

    void TestGraph();
 
private: 
 	
	Ptr<QKDBuffer>	buffer;
	uint32_t		m_sourceNode;
	uint32_t		m_destinationNode;
	uint32_t		m_sourceNodeDeviceId;
	uint32_t		m_destinationNodeDeviceId; 

	uint32_t        m_keymMin;
	uint32_t     	m_keymCurrent; 
	uint32_t        m_keymMax;
	uint32_t        m_tempMax;
	uint32_t     	m_keymThreshold;  

	std::string		m_plotFileName;
	std::string		m_plotFileType; //png or svg
	double			m_simulationTime;
	uint32_t		m_graphStatusEntry;

	Gnuplot				m_gnuplot;
    Gnuplot2dDataset 	m_dataset;
    Gnuplot2dDataset 	m_datasetWorkingState_Mthr;
    Gnuplot2dDataset 	m_datasetWorkingState_0;
    Gnuplot2dDataset 	m_datasetWorkingState_1;
    Gnuplot2dDataset 	m_datasetWorkingState_2;
    Gnuplot2dDataset 	m_datasetWorkingState_3;
    Gnuplot2dDataset 	m_datasetThreshold;
    Gnuplot2dDataset 	m_datasetMinimum;
    Gnuplot2dDataset 	m_datasetMaximum;
};
}

#endif /* QKDGRAPH */
