/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */
 
#ifndef QKDCrypto_H
#define QKDCrypto_H

#include <algorithm>
#include <stdint.h>

#include "ns3/header.h"
#include "ns3/tcp-header.h"
#include "ns3/udp-header.h" 
#include "ns3/icmpv4.h"

#include "ns3/dsdv-packet.h" 
#include "ns3/dsdvq-packet.h" 

#include "ns3/aodv-packet.h"
#include "ns3/aodvq-packet.h"

#include "ns3/olsr-header.h"
#include "ns3/olsrq-header.h"

#include "ns3/packet.h"
#include "ns3/packet-metadata.h"
#include "ns3/tag.h" 
#include "ns3/object.h"
#include "ns3/callback.h"
#include "ns3/assert.h"
#include "ns3/ptr.h"
#include "ns3/deprecated.h"
#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/qkd-buffer.h"
#include "ns3/qkd-header.h"
#include "ns3/qkd-key.h"
#include "ns3/net-device.h"

#include <crypto++/aes.h>
#include <crypto++/modes.h>
#include <crypto++/filters.h>
#include <crypto++/hex.h>
#include <crypto++/osrng.h>
#include <crypto++/ccm.h>
#include <crypto++/vmac.h>
#include <crypto++/iterhash.h>
#include <crypto++/secblock.h>
#include <crypto++/sha.h>
#include <vector>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <crypto++/md5.h> 

//ENCRYPTION
#define QKDCRYPTO_OTP 1
#define QKDCRYPTO_AES 2
//AUTHENTICATION
#define QKDCRYPTO_AUTH_VMAC     3
#define QKDCRYPTO_AUTH_MD5      4
#define QKDCRYPTO_AUTH_SHA1     5

namespace ns3 {

class QKDCrypto : public Object
{
public:

    struct CacheRecord
    {
        Ptr<Packet> fragment;
        uint32_t    messageID;
        uint32_t    offset;
        ~CacheRecord()
        {
            fragment = 0;
        }
    };


    QKDCrypto ();

    virtual ~QKDCrypto ();    
    /**
    * \brief Get the TypeId
    *
    * \return The TypeId for this class
    */
    static TypeId GetTypeId (void);
    
    std::vector<Ptr<Packet> > Encrypt(Ptr<Packet> p, Ptr<QKDBuffer>);

    Ptr<Packet> Decrypt (Ptr<Packet> p, Ptr<QKDBuffer> QKDBuffer);

    //Deprecated
    //Ptr<Packet> Reassembly (Ptr<Packet> p);

    std::vector<Ptr<Packet> > ProcessIncomingPacket (
        Ptr<Packet>     p, 
        Ptr<QKDBuffer>  QKDBuffer,
        uint32_t        channelID
    );

    std::vector<Ptr<Packet> > ProcessOutgoingPacket (
        Ptr<Packet>     p, 
        Ptr<QKDBuffer>  QKDBuffer,
        uint32_t        channelID
    );

    bool 
    CheckForResourcesToProcessThePacket(
        Ptr<Packet>             p, 
        uint32_t                TOSBand,
        Ptr<QKDBuffer>          QKDbuffer
    );

private:

    byte m_iv   [ CryptoPP::AES::BLOCKSIZE ];
 
    std::vector<QKDCrypto::CacheRecord > m_packetFragmentCache;

    QKDHeader StringToQKDHeader(std::string& input);

    QKDDelimiterHeader StringToQKDDelimiterHeader(std::string& input);

    std::string PacketToString (Ptr<Packet> p);

    std::vector<uint8_t> StringToVector(std::string& input);

    std::string VectorToString(std::vector<uint8_t> inputVector);

    std::vector<uint8_t> QKDHeaderToVector(QKDHeader& qkdHeader);

    std::vector<uint8_t> QKDDelimiterHeaderToVector(QKDDelimiterHeader& qkdHeader);

    std::string OTP (const std::string& data, Ptr<QKDKey> key);
    
    std::string AESEncrypt (const std::string& data, Ptr<QKDKey> key);

    std::string AESDecrypt (const std::string& data, Ptr<QKDKey> key);

    std::string Authenticate(std::string&, Ptr<QKDKey> key, uint8_t authenticationType);

    QKDCommandHeader CreateQKDCommandHeader(Ptr<Packet> p);

    Ptr<Packet> CheckAuthentication(Ptr<Packet> p, Ptr<QKDKey> key, uint8_t authenticationType);

    std::string HexEncode(const std::string& data);

    std::string HexDecode(const std::string& data);

    std::string base64_encode(std::string& s);

    std::string base64_decode(std::string const& s);

    std::string VMAC (std::string& inputString, Ptr<QKDKey> key);

    std::string MD5 (std::string& inputString);

    std::string SHA1 (std::string& inputString);

    std::string StringCompressEncode(const std::string& data);

    std::string StringDecompressDecode(const std::string& data);

    std::vector<Ptr<Packet> > CheckForFragmentation (
        Ptr<Packet>         p, 
        Ptr<QKDBuffer>      QKDBuffer
    );

    uint32_t    m_authenticationTagLengthInBits; 
    
    TracedCallback<Ptr<Packet> > m_encryptionTrace;
    TracedCallback<Ptr<Packet> > m_decryptionTrace;

    TracedCallback<Ptr<Packet>, std::string > m_authenticationTrace;
    TracedCallback<Ptr<Packet>, std::string > m_deauthenticationTrace;

    TracedCallback<Ptr<Packet> > m_routingProtocolPacketOutgoingTrace;
    TracedCallback<Ptr<Packet> > m_routingProtocolPacketIncomingTrace;

	std::map<uint32_t, std::string> m_cacheFlowValues;

    uint32_t m_qkdHeaderSize;
    uint32_t m_qkdDHeaderSize;

    bool m_compressionEnabled;
    bool m_encryptionEnabled;
     

    /////////////////////////////
    //FIXED HEADER SIZES
    ///////////////////////////////

    //IPv4
    uint32_t m_ipv4HeaderSize;

    //ICMPv4
    uint32_t m_icmpv4HeaderSize;
    uint32_t m_icmpv4EchoHeaderSize; 
    uint32_t m_icmpv4TimeExceededHeaderSize;
    uint32_t m_icmpv4DestinationUnreachableHeaderSize;
 
    //UDP
    uint32_t m_udpHeaderSize;

    //OLSR
    uint32_t m_olsrPacketHeaderSize;

    //OLSRQ
    uint32_t m_olsrqPacketHeaderSize;

    //DSDVQ
    uint32_t m_dsdvqHeaderSize;

    //DSDV
    uint32_t m_dsdvHeaderSize; 
        
    //AODV
    uint32_t m_aodvTypeHeaderSize;
    uint32_t m_aodvRrepHeaderSize;
    uint32_t m_aodvRreqHeaderSize;
    uint32_t m_aodvRrepAckHeaderSize;
    uint32_t m_aodvRerrHeaderSize;

    //AODVQ
    uint32_t m_aodvqTypeHeaderSize;
    uint32_t m_aodvqRrepHeaderSize;
    uint32_t m_aodvqRreqHeaderSize;
    uint32_t m_aodvqRrepAckHeaderSize;
    uint32_t m_aodvqRerrHeaderSize;

}; 
} // namespace ns3

#endif /* QKDCrypto_QKD_H */
