/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */
#include "packet.h"
#include "ns3/assert.h"
#include "ns3/log.h" 
#include <string>
#include <cstdarg>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("QKDKey");

uint32_t QKDKey::m_globalUid = 0;
  
Ptr<QKDKey> 
QKDKey::Copy (void) const
{
  // we need to invoke the copy constructor directly
  // rather than calling Create because the copy constructor
  // is private.
  return Ptr<QKDKey> (new QKDKey (*this), false);
}

QKDKey::QKDKey (uint32_t size)
  : m_size (size)
{
  m_globalUid++;
}
   
uint64_t 
QKDKey::GetUid (void) const
{
  return m_uid;
}
    
uint_t 
QKDKey::GetSize (void) const
{
  return m_size;
}

} // namespace ns3
