/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#ifndef QKD_MANAGER_H
#define QKD_MANAGER_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/tag.h"
#include "ns3/object.h"
#include "ns3/node.h"
#include "ns3/ipv4-header.h"
#include "ns3/qkd-buffer.h"
#include "ns3/net-device.h"
#include "ns3/qkd-crypto.h"
#include "ns3/qkd-net-device.h"

#include "ns3/internet-module.h"
#include "ns3/ipv4-interface-address.h"

#include <vector> 
#include <map>

namespace ns3 {

    struct NeigborDetail{ 
      uint32_t    interface;
      Address     sourceAddress; //MAC address of source netDevice
      float       distance; //Geographical distance
      float       linkPerformance; //link performance (status)
    };

    class Node; 
    class QKDNetDevice; 

    /**
     * \ingroup internet-queues
     *
     */
    class QKDManager : public Object {
    public:
 
        struct Connection
        {
            uint32_t                bufferId;
            Ptr<QKDNetDevice>       QKDNetDeviceSrc;
            Ptr<QKDNetDevice>       QKDNetDeviceDst;
            Ptr<NetDevice>          IPNetDeviceSrc;
            Ptr<NetDevice>          IPNetDeviceDst;
            Ptr<QKDBuffer>          buffer;
            Ptr<QKDCrypto>          crypto;
            Ptr<Socket>             socket;
            Ptr<Socket>             socketSink;
            std::string             socketType;
            uint32_t                underlayPortNumber; 
            Ipv4InterfaceAddress    IPQKDSrc; //QKD IP Src Address - overlay device
            Ipv4InterfaceAddress    IPQKDDst; //QKD IP Dst Address - overlay device
            Ipv4InterfaceAddress    IPSrc;  //IP Src Address - underlay device
            Ipv4InterfaceAddress    IPDst;  //IP Dst Address - underlay device
            bool                    isMaster; 
            bool                    shouldEncrypt;
            bool                    shouldAuthenticate;
            uint32_t                channelID;
            double                  publicChannelMetric;
            double                  quantumChannelMetric; 

            ~Connection()
            {
                QKDNetDeviceSrc = 0;
                QKDNetDeviceDst = 0;
                IPNetDeviceSrc = 0;
                IPNetDeviceDst = 0;
                buffer = 0;
                crypto = 0;
                socket = 0;
                socketSink = 0;
            }
        };

        struct smallDetail
        {
            Ptr<Node>       destinationNode;
            Ipv4Address     sourceAddress;
            Ipv4Address     destinationAddress;

            ~smallDetail()
            {
                destinationNode = 0; 
            }

        };
 
        /**
        * \brief Get the type ID.
        * \return the object TypeId
        */
        static TypeId GetTypeId (void);
       
        QKDManager ();

        virtual ~QKDManager ();
   
        /**
        * \returns the number of QKDNetDevice instances associated
        *          to this Node.
        */
        uint32_t GetNBuffers (void) const;
 
        void AddNewLink ( 
            Ptr<QKDNetDevice>       NetDeviceSrc,
            Ptr<QKDNetDevice>       NetDeviceDst,
            Ptr<NetDevice>          IPNetDeviceSrc,
            Ptr<NetDevice>          IPNetDeviceDst,
            Ptr<QKDCrypto>          Crypto,
            Ptr<Socket>             socket,
            Ptr<Socket>             socketSink,
            std::string             socketType,
            uint32_t                underlayPortNumber, 
            Ipv4InterfaceAddress    IPQKDSrc, //QKD IP Src Address - overlay device
            Ipv4InterfaceAddress    IPQKDDst, //QKD IP Dst Address - overlay device 
            Ipv4InterfaceAddress    IPSrc,  //IP Src Address - underlay device
            Ipv4InterfaceAddress    IPDst,  //IP Dst Address - underlay device 
            bool                    isMaster,  
            uint32_t                Mmin, 
            uint32_t                Mthr, 
            uint32_t                Mmax, 
            uint32_t                Mcurrent,
            uint32_t                channelID
        );


        std::vector<Ptr<Packet> > ProcessIncomingRequest (Ptr<NetDevice> src, Ptr<Packet> p);

        std::vector<Ptr<Packet> > ProcessOutgoingRequest (Ptr<NetDevice> src, Ptr<Packet> p); 
        
        Ptr<Node> GetDestinationNode (const Address dst);
 
        bool AddNewKeyMaterial (const Address sourceAddress, uint32_t& newKey);
     
        uint8_t FetchStatusForDestinationBuffer(Ptr<NetDevice> src);

        uint32_t FetchMaxNumberOfRecordedKeyChargingTimePeriods (Ipv4Address nextHop);
    
        void VirtualReceive  (Ptr<Socket> socket);
        
        bool VirtualSendOverlay (
            Ptr<Packet> packet, 
            const Address& source, 
            const Address& dest, 
            uint16_t protocolNumber
        );

        bool VirtualSend (
            Ptr<Packet> packet, 
            const Address& source, 
            const Address& dest, 
            uint16_t protocolNumber,
            uint8_t TxQueueIndex
        );
 
        bool
        ForwardToSocket(
            Ptr<Packet> originalPacket,
            Ptr<Packet> packet, 
            const Address& source, 
            const Address& dst,
            uint16_t protocolNumber,
            uint8_t TxQueueIndex
        );

        bool
        CheckForResourcesToProcessThePacket(
            Ptr<Packet> p,
            const Address sourceAddress
        );
         
        bool
        CheckForResourcesToProcessThePacket(
            Ptr<Packet> p,
            const Address sourceAddress,
            const uint32_t& TOSband 
        );

        uint32_t GetBufferPosition (const Address& sourceAddress);

        Ptr<QKDBuffer> GetBufferByBufferPosition (const uint32_t& index);

        Ptr<QKDBuffer> GetBufferBySourceAddress (const Address& sourceAddress);

        QKDManager::Connection GetConnectionDetails (const uint32_t& bufferId);

        QKDManager::Connection GetConnectionDetails (const Address sourceAddress);

        Ptr<Packet> MarkAuthenticate (Ptr<Packet> p);

        Ptr<Packet> MarkEncrypt (Ptr<Packet> p);

        Ptr<Packet> MarkEncrypt (Ptr<Packet> p, uint8_t encryptionType, uint8_t authneticationType);

        Ptr<Packet> MarkMaxDelay(Ptr<Packet> p, uint32_t delay);

        bool IsMarkedAsEncrypt (Ptr<Packet> p);

        Ptr<Packet> Decrypt (Ptr<Packet> p, Address dst);

        Ptr<Packet> Authenticate (Ptr<Packet> p, Address dst);

        Ptr<Packet> CheckAuthenication (Ptr<Packet> p, Address dst);
  
        void        TestBuffer (Ptr<QKDBuffer> buffer);

        void ConnectionSucceeded (Ptr<Socket> socket);

        void ConnectionFailed (Ptr<Socket> socket);
        /**
        * \brief Handle an incoming connection
        * \param socket the incoming connection socket
        * \param from the address the connection is from
        */
        void HandleAccept (Ptr<Socket> socket, const Address& from);

        void UseRealStorages (const bool& useRealStorages);

        void VirtualReceiveSimpleNetwork (Ptr<NetDevice> device, Ptr<const Packet> p,
                        uint16_t protocol, const Address &from,
                        const Address &to, NetDevice::PacketType packetType
        );
 
        uint32_t GetNumberOfDestinations();
 
        std::vector<QKDManager::smallDetail > GetMapOfSourceDestinationAddresses();

        uint32_t    FetchLinkThresholdHelpValue(); 

        void        CalculateLinkThresholdHelpValue();

        uint32_t GetLinkThresholdValue(const Address sourceAddress );

        void SetLinkThresholdValue( const uint32_t& proposedLaValue, const Address sourceAddress );

        Ipv4Address PopulateLinkStatusesForNeighbors( 
            Ptr<Packet> p, 
            std::map<Ipv4Address, NeigborDetail> distancesToDestination,
            uint8_t tos,
            uint32_t& outputInterface 
        );

        uint32_t    FetchPacketTos(Ptr<Packet> p);

        uint32_t    TosToBand(const uint32_t& tos);

        Ptr<NetDevice>  GetSourceNetDevice (const Address address);

        std::map<Address, QKDManager::Connection >::iterator FetchConnection(const Address sourceAddress);

        double  FetchPublicChannelPerformance (Ipv4Address nextHop);
 
    protected: 
      /**
       * The dispose method. Subclasses must override this method
       * and must chain up to it by calling Node::DoDispose at the
       * end of their own DoDispose method.
       */
      virtual void DoDispose (void);

      virtual void DoInitialize (void);

    private:  
  
        void UpdateQuantumChannelMetric(const Address sourceAddress);

        void UpdatePublicChannelMetric (const Address sourceAddress);

        uint32_t    m_linksThresholdHelpValue;

        bool    m_useRealStorages;

    	std::map<Address, Connection> m_destinations; //map of QKD destinations including buffers

        std::vector<Ptr<QKDBuffer> > m_buffers; // Buffers associated to this QKD manager

        Ipv4Mask m_ipv4Mask;
    }; 
}  
// namespace ns3

#endif /* PriorityQueue */
