/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#include "ns3/qkd-graph-manager.h" 
 
namespace ns3 {
 
NS_LOG_COMPONENT_DEFINE ("QKDGraphManager");

NS_OBJECT_ENSURE_REGISTERED (QKDGraphManager);

TypeId QKDGraphManager::GetTypeId (void) 
{
  static TypeId tid = TypeId ("ns3::QKDGraphManager")
    .SetParent<Object> () 
    ; 
  return tid;
}
  
bool QKDGraphManager::instanceFlag = false;
QKDGraphManager* QKDGraphManager::single = NULL;
QKDGraphManager* QKDGraphManager::getInstance()
{
    if(!instanceFlag){
		single = new QKDGraphManager();
		instanceFlag = true;
    }
    return single;
}


QKDGraphManager::~QKDGraphManager(){
	instanceFlag = false;
}

void
QKDGraphManager::PrintGraphs(){ 

    NS_LOG_FUNCTION (this);
	for (std::vector<std::vector<QKDGraph *> >::iterator i = m_graphs.begin();i != m_graphs.end(); ++i)
	{   
		for (std::vector<QKDGraph *>::iterator j = i->begin(); j != i->end(); ++j)
        { 
            if((*j)!=0)
            {
		        (*j)->PrintGraph(); 
            }
        }
	} 
}

void
QKDGraphManager::SendCurrentChangeValueToGraph(uint32_t nodeID, uint32_t bufferID, uint32_t value){

    NS_LOG_FUNCTION (this << nodeID << bufferID << value);
	m_graphs[nodeID][bufferID]->ProcessMCurrent(value);
}

void
QKDGraphManager::SendStatusValueToGraph(uint32_t nodeID, uint32_t bufferID, uint32_t value){

    NS_LOG_FUNCTION (this << nodeID << bufferID << value);
	m_graphs[nodeID][bufferID]->ProcessMStatus(value);
}

void 
QKDGraphManager::ProcessCurrentChange(std::string context, uint32_t value)
{ 
	int nodeId; 
	int bufferId;
	std::sscanf(context.c_str(), "/NodeList/%d/$ns3::QKDManager/BufferList/%d/*", &nodeId, &bufferId);

	std::cout << Simulator::Now() << "\t" << context << value << "\t\n" ;

	QKDGraphManager *manager = QKDGraphManager::getInstance();
	manager->SendCurrentChangeValueToGraph (nodeId, bufferId, value); 
}

void 
QKDGraphManager::ProcessStatusChange(std::string context, uint32_t value)
{
	//NodeList/0/$ns3::QKDManager/BufferList/0/CurrentChange
	int nodeId; 
	int bufferId;
	std::sscanf(context.c_str(), "/NodeList/%d/$ns3::QKDManager/BufferList/%d/*", &nodeId, &bufferId);

	QKDGraphManager *manager = QKDGraphManager::getInstance();
	manager->SendStatusValueToGraph (nodeId, bufferId, value); 
}
  

void 
QKDGraphManager::AddBuffer(uint32_t nodeID, uint32_t bufferID)
{ 		  
	AddBuffer(nodeID, bufferID, "");
}

void 
QKDGraphManager::AddBuffer(uint32_t nodeID, uint32_t bufferID, std::string graphName)
{ 	    
    NS_LOG_FUNCTION (this << nodeID << bufferID << graphName);
	  
	if(m_graphs.size() <= nodeID)
		m_graphs.resize(nodeID+1);
	
	if(m_graphs.size() == 0)
		m_graphs[nodeID] = std::vector<QKDGraph *> ();

	if(m_graphs[nodeID].size() <= bufferID)
		m_graphs[nodeID].resize(bufferID+1);
	
	m_graphs[nodeID][bufferID] = new QKDGraph (nodeID, bufferID, graphName);


	std::ostringstream currentPath;
	currentPath << "/NodeList/" << nodeID << "/$ns3::QKDManager/BufferList/" << bufferID << "/CurrentChange"; 

	std::string query(currentPath.str());
    Config::Connect(query, MakeCallback (&QKDGraphManager::ProcessCurrentChange));
    NS_LOG_FUNCTION (this << query);


	 
	std::ostringstream statusPath; 
	statusPath << "/NodeList/" << nodeID << "/$ns3::QKDManager/BufferList/" << bufferID << "/StatusChange";

	std::string query2(statusPath.str());
    Config::Connect(query2, MakeCallback (&QKDGraphManager::ProcessStatusChange)); 
    NS_LOG_FUNCTION (this << query2);
}
}
