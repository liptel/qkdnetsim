/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/log.h"
#include "qkd-drop-tail-queue.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("QKDDropTailQueue");

NS_OBJECT_ENSURE_REGISTERED (QKDDropTailQueue);

TypeId QKDDropTailQueue::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::QKDDropTailQueue")
    .SetParent<Queue> ()
    .SetGroupName ("Network")
    .AddConstructor<QKDDropTailQueue> ()
  ;
  return tid;
}

QKDDropTailQueue::QKDDropTailQueue () :
  Queue (),
  m_packets ()
{
  NS_LOG_FUNCTION (this);
}

QKDDropTailQueue::~QKDDropTailQueue ()
{
  NS_LOG_FUNCTION (this);
}

bool 
QKDDropTailQueue::DoEnqueue (Ptr<QueueItem> item)
{
  NS_LOG_FUNCTION (this << item << m_packets.size ());

  if((m_packets.size () + 1)  == GetNPackets ())
    m_packets.pop ();
  
  NS_ASSERT (m_packets.size () == GetNPackets ());

  m_packets.push (item);

  return true;
}

Ptr<QueueItem>
QKDDropTailQueue::DoDequeue (void)
{
  NS_LOG_FUNCTION (this << m_packets.size ());

  if((m_packets.size () + 1)  == GetNPackets ())
    m_packets.pop ();

  NS_ASSERT (m_packets.size () == GetNPackets ());

  Ptr<QueueItem> item = m_packets.front ();
  m_packets.pop ();

  NS_LOG_LOGIC ("Popped " << item);

  return item;
}

Ptr<QueueItem>
QKDDropTailQueue::DoRemove (void)
{
  NS_LOG_FUNCTION (this << m_packets.size ());
  NS_ASSERT (m_packets.size () == GetNPackets ());

  Ptr<QueueItem> item = m_packets.front ();
  m_packets.pop ();

  NS_LOG_LOGIC ("Removed " << item);

  return item;
}

Ptr<const QueueItem>
QKDDropTailQueue::DoPeek (void) const
{
  NS_LOG_FUNCTION (this << m_packets.size ());
  NS_ASSERT (m_packets.size () == GetNPackets ());

  return m_packets.front ();
}

} // namespace ns3

