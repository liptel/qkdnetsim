/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */


// Network topology
//
//       n0 ---p2p-- n1 --p2p-- n2
//        |---------qkd---------|
//
// - udp flows from n0 to n2

#include <fstream>
#include "ns3/core-module.h" 
#include "ns3/qkd-helper.h" 
#include "ns3/qkd-app-charging-helper.h"
#include "ns3/qkd-graph-manager.h" 
#include "ns3/applications-module.h"
#include "ns3/internet-module.h" 
#include "ns3/flow-monitor-module.h" 
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/gnuplot.h" 
#include "ns3/qkd-send.h"
#include "ns3/random-variable-stream.h" 
#include "ns3/aodv-module.h"
#include "ns3/olsr-module.h"
#include "ns3/dsdv-module.h"
#include "ns3/dsr-module.h"

#include "ns3/aodvq-module.h" 
#include "ns3/dsdvq-module.h" 
#include "ns3/olsrq-module.h" 

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("QKD_CHANNEL_TEST");
   

// Measure Throughput

uint32_t m_bytes_total = 0; 
uint32_t m_bytes_received = 0; 
uint32_t m_bytes_sent = 0; 
uint32_t m_packets_received = 0; 
double m_time = 0;

std::map<uint32_t, double> m_delayTable;
  
void
SentPacket(std::string context, Ptr<const Packet> p){
    
    //std::cout << "Received: \t" << (double)Simulator::Now().GetSeconds() << "\n";

    /*
    std::cout << "\n ..................SentPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    m_bytes_sent += p->GetSize(); 
    //m_delayTable.insert (std::make_pair (p->GetUid(), (double)Simulator::Now().GetSeconds()));
}

void
ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){
    
    /*  
    std::cout << "\n ..................ReceivedPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  

    
    //double temp = (double)Simulator::Now().GetSeconds();  
    std::map<uint32_t, double >::iterator i = m_delayTable.find ( p->GetUid() );
    //std::cout << i->first << "\t" << temp - i->second << "\n";

    if(i != m_delayTable.end()){
        m_delayTable.erase(i);
    }
    */
    //std::cout << temp << "\t" << temp - m_time << "\n";
    //m_time = temp;
    //std::cout << "Received: \t" << (double)Simulator::Now().GetSeconds() << "\t" << p->GetSize() << "\n";
    
    //std::cout << "Received: \t" << (double)Simulator::Now().GetSeconds() << "\n";
    m_bytes_received += p->GetSize(); 
    m_bytes_total += p->GetSize(); 
    m_packets_received++;

}

void
ThroughputPerSecond (Ptr<Application> sink1Apps, int totalPacketsThrough, float prevThroughput, Ptr<Node> node){

    double throughput = 0.0;
    Ptr<QKDSink> sink1 = DynamicCast<QKDSink> (sink1Apps);

    totalPacketsThrough = sink1->GetTotalRx ();
    throughput = (totalPacketsThrough*8/(1000000.0)) - prevThroughput;
    prevThroughput = (totalPacketsThrough*8)/(1000000.0);

    std::cout << (Simulator::Now ()).GetSeconds () << "\t"<< throughput <<std::endl;

    Simulator::Schedule (Seconds (1.0), &ThroughputPerSecond, sink1Apps, totalPacketsThrough, prevThroughput, node);
}




void
Ratio(uint32_t m_bytes_sent, uint32_t m_packets_sent ){
    std::cout << "Sent (bytes):\t" <<  m_bytes_sent
    << "\tReceived (bytes):\t" << m_bytes_received 
    << "\nSent (Packets):\t" <<  m_packets_sent
    << "\tReceived (Packets):\t" << m_packets_received 
    
    << "\nRatio (bytes):\t" << (float)m_bytes_received/(float)m_bytes_sent
    << "\tRatio (packets):\t" << (float)m_packets_received/(float)m_packets_sent << "\n";
}

uint32_t MacTxDropCount, MacRxDropCount, PhyRxDropCount, PhyTxDropCount = 0;

void
MacTxDrop(Ptr<const Packet> p)
{
  NS_LOG_INFO("Packet Drop");
  MacTxDropCount++;
}
void
MacRxDrop(Ptr<const Packet> p)
{
  NS_LOG_INFO("Packet Drop");
  MacRxDropCount++;
}
void
PhyTxDrop(Ptr<const Packet> p)
{
  NS_LOG_INFO("Packet Drop");
  PhyTxDropCount++;
}
void
PhyRxDrop(Ptr<const Packet> p)
{
  NS_LOG_INFO("Packet Drop");
  PhyRxDropCount++;
}


void
PrintDrop()
{
  std::cout << Simulator::Now().GetSeconds() << "\t" << MacTxDropCount << "\t"<< MacRxDropCount << "\t" << PhyTxDropCount << "\t" << PhyRxDropCount << "\n";
  Simulator::Schedule(Seconds(5.0), &PrintDrop);
}

 
int main (int argc, char *argv[])
{
    Packet::EnablePrinting(); 
    PacketMetadata::Enable ();

    bool enableFlowMonitor = false;  
    double packetSize = 1024; 
    double errorRate = 0; 

    CommandLine cmd;
    cmd.AddValue ("EnableMonitor", "Enable Flow Monitor", enableFlowMonitor);
    cmd.AddValue ("PacketSize", "SetPacketSize", packetSize);
    cmd.AddValue ("ErrorRate", "errorRate", errorRate);
    cmd.Parse (argc, argv);

    //std::cout << packetSize << std::endl;

    //TCP SEGMENT SIZE
    Config::SetDefault ("ns3::RateErrorModel::ErrorRate", DoubleValue (errorRate));
    Config::SetDefault ("ns3::RateErrorModel::ErrorUnit", StringValue ("ERROR_UNIT_PACKET")); 
    Config::SetDefault ("ns3::RateErrorModel::RanVar", StringValue ("ns3::UniformRandomVariable[Min=0|Max=1]"));  // 4 MB of TCP buffer 

    //Config::SetDefault ("ns3::BurstErrorModel::ErrorRate", DoubleValue (0.01));
    //Config::SetDefault ("ns3::BurstErrorModel::BurstSize", StringValue ("ns3::UniformRandomVariable[Min=1|Max=3]"));

    //
    // Explicitly create the nodes required by the topology (shown above).
    //
    NS_LOG_INFO ("Create nodes.");
    NodeContainer n;
    n.Create (10); 

    NodeContainer n0n1 = NodeContainer (n.Get(0), n.Get (1));
    NodeContainer n1n2 = NodeContainer (n.Get(1), n.Get (2));
    NodeContainer n2n3 = NodeContainer (n.Get(2), n.Get (3));
    NodeContainer n3n4 = NodeContainer (n.Get(3), n.Get (4));
    NodeContainer n4n5 = NodeContainer (n.Get(4), n.Get (5));
    NodeContainer n5n6 = NodeContainer (n.Get(5), n.Get (6));
    NodeContainer n6n7 = NodeContainer (n.Get(6), n.Get (7));
    NodeContainer n7n8 = NodeContainer (n.Get(7), n.Get (8));
    NodeContainer n9n7 = NodeContainer (n.Get(9), n.Get (7));
    NodeContainer n2n9 = NodeContainer (n.Get(2), n.Get (9));
    
    NodeContainer qkdNodes;
    qkdNodes.Add(n.Get(0)); //0
    qkdNodes.Add(n.Get (2));//1
    qkdNodes.Add(n.Get (4));//2
    qkdNodes.Add(n.Get (7));//3
    qkdNodes.Add(n.Get (9));//4
    qkdNodes.Add(n.Get (8));//5

    //Enable OLSR
    //OlsrHelper routingProtocol;
    //DsdvHelper routingProtocol; 
      
    InternetStackHelper internet;
    //internet.SetRoutingHelper (routingProtocol);
    internet.Install (n);

    // Set Mobility for all nodes
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject <ListPositionAllocator>();
    positionAlloc ->Add(Vector(0, 200, 0));     // node0 
    positionAlloc ->Add(Vector(100, 200, 0));   // node1
    positionAlloc ->Add(Vector(200, 200, 0));   // node2 
    positionAlloc ->Add(Vector(250, 250, 0));   // node3
    positionAlloc ->Add(Vector(300, 300, 0));   // node4
    positionAlloc ->Add(Vector(350, 250, 0));   // node5
    positionAlloc ->Add(Vector(400, 220, 0));   // node6
    positionAlloc ->Add(Vector(450, 200, 0));   // node7
    positionAlloc ->Add(Vector(600, 200, 0));   // node8
    positionAlloc ->Add(Vector(300, 0, 0));     // node9

    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(n);
       
    // We create the channels first without any IP addressing information
    NS_LOG_INFO ("Create channels.");
    PointToPointHelper p2p;
    p2p.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
    p2p.SetChannelAttribute ("Delay", StringValue ("2ms")); 

    NetDeviceContainer d0d1 = p2p.Install (n0n1); 
    NetDeviceContainer d1d2 = p2p.Install (n1n2);
    NetDeviceContainer d2d3 = p2p.Install (n2n3);
    NetDeviceContainer d3d4 = p2p.Install (n3n4);
    NetDeviceContainer d4d5 = p2p.Install (n4n5);
    NetDeviceContainer d5d6 = p2p.Install (n5n6);
    NetDeviceContainer d6d7 = p2p.Install (n6n7);
    NetDeviceContainer d7d8 = p2p.Install (n7n8);
    NetDeviceContainer d9d7 = p2p.Install (n9n7);
    NetDeviceContainer d2d9 = p2p.Install (n2n9);
 
    //
    // We've got the "hardware" in place.  Now we need to add IP addresses.
    // 
    NS_LOG_INFO ("Assign IP Addresses.");
    Ipv4AddressHelper ipv4;

    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer i0i1 = ipv4.Assign (d0d1); 

    ipv4.SetBase ("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer i1i2 = ipv4.Assign (d1d2);

    ipv4.SetBase ("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer i2i3 = ipv4.Assign (d2d3);

    ipv4.SetBase ("10.1.4.0", "255.255.255.0");
    Ipv4InterfaceContainer i3i4 = ipv4.Assign (d3d4);

    ipv4.SetBase ("10.1.5.0", "255.255.255.0");
    Ipv4InterfaceContainer i4i5 = ipv4.Assign (d4d5);

    ipv4.SetBase ("10.1.6.0", "255.255.255.0");
    Ipv4InterfaceContainer i5i6 = ipv4.Assign (d5d6);

    ipv4.SetBase ("10.1.7.0", "255.255.255.0");
    Ipv4InterfaceContainer i6i7 = ipv4.Assign (d6d7);

    ipv4.SetBase ("10.1.8.0", "255.255.255.0");
    Ipv4InterfaceContainer i7i8 = ipv4.Assign (d7d8);

    ipv4.SetBase ("10.1.9.0", "255.255.255.0");
    Ipv4InterfaceContainer i9i7 = ipv4.Assign (d9d7);

    ipv4.SetBase ("10.1.10.0", "255.255.255.0");
    Ipv4InterfaceContainer i2i9 = ipv4.Assign (d2d9); 

    // Create router nodes, initialize routing database and set up the routing
    // tables in the nodes.
    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    ObjectFactory factory; 
    factory.SetTypeId ("ns3::RateErrorModel");

    Ptr<ErrorModel> em0 = factory.Create<ErrorModel> ();
    d0d1.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em0));

    Ptr<ErrorModel> em1 = factory.Create<ErrorModel> ();
    d0d1.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));

    Ptr<ErrorModel> em2 = factory.Create<ErrorModel> ();
    d1d2.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em2));

    Ptr<ErrorModel> em3 = factory.Create<ErrorModel> ();
    d1d2.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em3));

    Ptr<ErrorModel> em4 = factory.Create<ErrorModel> ();
    d2d3.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em4));

    Ptr<ErrorModel> em5 = factory.Create<ErrorModel> ();
    d2d3.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em5));

    Ptr<ErrorModel> em6 = factory.Create<ErrorModel> ();
    d3d4.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em6));

    Ptr<ErrorModel> em7 = factory.Create<ErrorModel> ();
    d3d4.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em7));

    Ptr<ErrorModel> em8 = factory.Create<ErrorModel> ();
    d4d5.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em8));

    Ptr<ErrorModel> em9 = factory.Create<ErrorModel> ();
    d4d5.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em9));

    Ptr<ErrorModel> em10 = factory.Create<ErrorModel> ();
    d5d6.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em10));

    Ptr<ErrorModel> em11 = factory.Create<ErrorModel> ();
    d5d6.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em11));

    Ptr<ErrorModel> em12 = factory.Create<ErrorModel> ();
    d7d8.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em12));

    Ptr<ErrorModel> em13 = factory.Create<ErrorModel> ();
    d7d8.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em13));

    Ptr<ErrorModel> em14 = factory.Create<ErrorModel> ();
    d9d7.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em14));

    Ptr<ErrorModel> em15 = factory.Create<ErrorModel> ();
    d9d7.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em15));

    Ptr<ErrorModel> em16 = factory.Create<ErrorModel> ();
    d2d9.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em16));

    Ptr<ErrorModel> em17 = factory.Create<ErrorModel> ();
    d2d9.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em17));

    //Enable Overlay Routing
    //AodvqHelper routingOverlayProtocol;
    OlsrqHelper routingOverlayProtocol;
    //DsdvqHelper routingOverlayProtocol; 

    //
    // Explicitly create the channels required by the topology (shown above).
    //
    QKDHelper QHelper; 

    //install QKD Managers on the nodes
    QHelper.SetRoutingHelper (routingOverlayProtocol);
    QHelper.InstallQKDManager (qkdNodes); 
        
    //Create QKDNetDevices and create QKDbuffers
    Ipv4InterfaceAddress va02_0 (Ipv4Address ("11.0.0.1"), Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va02_2 (Ipv4Address ("11.0.0.2"), Ipv4Mask ("255.255.255.0"));

    Ipv4InterfaceAddress va24_2 (Ipv4Address ("11.0.0.3"), Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va24_4 (Ipv4Address ("11.0.0.4"), Ipv4Mask ("255.255.255.0"));

    Ipv4InterfaceAddress va47_4 (Ipv4Address ("11.0.0.5"), Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va47_7 (Ipv4Address ("11.0.0.6"), Ipv4Mask ("255.255.255.0"));

    Ipv4InterfaceAddress va29_2 (Ipv4Address ("11.0.0.7"), Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va29_9 (Ipv4Address ("11.0.0.8"), Ipv4Mask ("255.255.255.0"));

    Ipv4InterfaceAddress va97_9 (Ipv4Address ("11.0.0.9"),  Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va97_7 (Ipv4Address ("11.0.0.10"), Ipv4Mask ("255.255.255.0"));
 
    Ipv4InterfaceAddress va78_7 (Ipv4Address ("11.0.0.11"), Ipv4Mask ("255.255.255.0"));
    Ipv4InterfaceAddress va78_8 (Ipv4Address ("11.0.0.12"), Ipv4Mask ("255.255.255.0"));
    
    //create QKD connection between nodes 0 and 2
    NetDeviceContainer qkdNetDevices02 = QHelper.InstallOverlayQKD (
        d0d1.Get(0), d1d2.Get(1), 
        va02_0, va02_2, 
        15485700,    //min
        0,   //thr
        52428800,   //max
        40485770     //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(0), d0d1.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 
    
    
    //create QKD connection between nodes 2 and 4
    NetDeviceContainer qkdNetDevices24 = QHelper.InstallOverlayQKD (
        d2d3.Get(0), d3d4.Get(1), 
        va24_2, va24_4,  
        15485700,    //min
        0,   //thr
        52428800,   //max
        20485770     //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(2), d2d3.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 

    
    
    //create QKD connection between nodes 4 and 7
    NetDeviceContainer qkdNetDevices47 = QHelper.InstallOverlayQKD (
        d4d5.Get(0), d6d7.Get(1), 
        va47_4, va47_7, 
        15485700,    //min
        0,   //thr
        52428800,   //max
        20485770    //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(4),  d4d5.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 

    //create QKD connection between nodes 9 and 7
    NetDeviceContainer qkdNetDevices97 = QHelper.InstallOverlayQKD (
        d9d7.Get(0), d9d7.Get(1), 
        va97_9, va97_7, 
        15485700,    //min
        0,   //thr
        52428800,   //max
        15885700     //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(9), d9d7.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 
    

    //create QKD connection between nodes 2 and 9
    NetDeviceContainer qkdNetDevices29 = QHelper.InstallOverlayQKD (
        d2d9.Get(0), d2d9.Get(1), 
        va29_2, va29_9, 
        15485700,    //min
        0,   //thr
        52428800,   //max
        20485770     //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(2), d2d9.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 


    //create QKD connection between nodes 7 and 8
    NetDeviceContainer qkdNetDevices78 = QHelper.InstallOverlayQKD (
        d7d8.Get(0), d7d8.Get(1), 
        va78_7, va78_8, 
        15485700,    //min 
        0,   //thr
        52428800,   //max
        40485770     //current    //20485770
    );
    //Create graph to monitor buffer changes
    QHelper.AddGraph(n.Get(7), d7d8.Get(0) ); //srcNode, destinationAddress, GraphTitle, GraphType (svg or png) 
      


    //if needed, routing protocol can be installed on overlay level
    //routingOverlayProtocol.InstallOnNodes (qkdNodes);

    NS_LOG_INFO ("Create Applications.");

    /* QKD APPs for charing */
    QKDAppChargingHelper qkdChargingApp02("ns3::VirtualTcpSocketFactory", va02_0.GetLocal (), va02_2.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps02 = qkdChargingApp02.Install ( qkdNetDevices02.Get (0), qkdNetDevices02.Get (1) );
    qkdChrgApps02.Start (Seconds (5.));
    qkdChrgApps02.Stop (Seconds (300.));
    
    QKDAppChargingHelper qkdChargingApp24("ns3::VirtualTcpSocketFactory", va24_2.GetLocal (), va24_4.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps24 = qkdChargingApp24.Install ( qkdNetDevices24.Get (0), qkdNetDevices24.Get (1) );
    qkdChrgApps24.Start (Seconds (5.));
    qkdChrgApps24.Stop (Seconds (300.));
 
    QKDAppChargingHelper qkdChargingApp47("ns3::VirtualTcpSocketFactory", va47_4.GetLocal (), va47_7.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps47 = qkdChargingApp47.Install ( qkdNetDevices47.Get (0), qkdNetDevices47.Get (1) );
    qkdChrgApps47.Start (Seconds (5.));
    qkdChrgApps47.Stop (Seconds (300.)); 

    QKDAppChargingHelper qkdChargingApp29("ns3::VirtualTcpSocketFactory", va29_2.GetLocal (), va29_9.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps29 = qkdChargingApp29.Install ( qkdNetDevices29.Get (0), qkdNetDevices29.Get (1) );
    qkdChrgApps29.Start (Seconds (5.));
    qkdChrgApps29.Stop (Seconds (300.)); 

    QKDAppChargingHelper qkdChargingApp97("ns3::VirtualTcpSocketFactory", va97_9.GetLocal (), va97_7.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps97 = qkdChargingApp97.Install ( qkdNetDevices97.Get (0), qkdNetDevices97.Get (1) );
    qkdChrgApps97.Start (Seconds (5.));
    qkdChrgApps97.Stop (Seconds (300.)); 

    QKDAppChargingHelper qkdChargingApp78("ns3::VirtualTcpSocketFactory", va78_7.GetLocal (), va78_8.GetLocal (), 3072000);
    ApplicationContainer qkdChrgApps78 = qkdChargingApp78.Install ( qkdNetDevices78.Get (0), qkdNetDevices78.Get (1) );
    qkdChrgApps78.Start (Seconds (5.));
    qkdChrgApps78.Stop (Seconds (300.)); 
    
    std::cout << "Source IP address: " << va02_0.GetLocal () << std::endl;
    std::cout << "Destination IP address: " << va78_8.GetLocal () << std::endl;
    
    /* Create user's traffic between v0 and v8 */
    /* Create sink app */
    uint16_t sinkPort = 8080;
    QKDSinkAppHelper packetSinkHelper ("ns3::VirtualUdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), sinkPort));
    ApplicationContainer sinkApps = packetSinkHelper.Install (n.Get (8));
    sinkApps.Start (Seconds (50.));
    sinkApps.Stop (Seconds (80.)); 
   
    
    /* Create source app */
    Address sinkAddress (InetSocketAddress (va78_8.GetLocal (), sinkPort)); 
    Address sourceAddress (InetSocketAddress (va02_0.GetLocal (), sinkPort));
    Ptr<Socket> overlaySocket = Socket::CreateSocket (n.Get (0), VirtualUdpSocketFactory::GetTypeId ());
 
    Ptr<QKDSend> app = CreateObject<QKDSend> ();
    app->Setup (overlaySocket, sourceAddress, sinkAddress, packetSize, 0, DataRate ("10Mbps")); //10000000
    n.Get (0)->AddApplication (app);
    app->SetStartTime (Seconds (50.));
    app->SetStopTime (Seconds (80.)); 
    
     
    //////////////////////////////////////
    ////         STATISTICS
    //////////////////////////////////////

    //if we need we can create pcap files
    //p2p.EnablePcapAll ("QKD_overlay_test"); 
    //QHelper.EnablePcapAll ("QKD_overlay_level_test"); 

    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::QKDSend/Tx", MakeCallback(&SentPacket)); // aggregate throughput 
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::QKDSink/Rx", MakeCallback(&ReceivedPacket)); // aggregate throughput
  
    // Trace Collisions
    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::PointToPointNetDevice/MacTxDrop", MakeCallback(&MacTxDrop));
    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::PointToPointNetDevice/MacRxDrop", MakeCallback(&MacRxDrop));

    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::PointToPointNetDevice/PhyTxDrop", MakeCallback(&PhyTxDrop));
    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::PointToPointNetDevice/PhyRxDrop", MakeCallback(&PhyRxDrop));

 
    //flowMonitor declaration
    FlowMonitorHelper fmHelper;
    Ptr<FlowMonitor> allMon = fmHelper.InstallAll();

    allMon->CheckForLostPackets ();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (fmHelper.GetClassifier ());
 
    Simulator::Stop (Seconds (135));
    Simulator::Run ();

    Ratio(app->sendDataStats(), app->sendPacketStats());
  
    QHelper.PrintGraphs();
    Simulator::Destroy ();
}
