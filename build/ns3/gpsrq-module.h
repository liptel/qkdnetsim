
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_GPSRQ
    

// Module headers:
#include "gpsrq-helper.h"
#include "gpsrq-packet.h"
#include "gpsrq-ptable.h"
#include "gpsrq-rqueue.h"
#include "gpsrq.h"
#endif
