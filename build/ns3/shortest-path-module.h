
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_SHORTEST_PATH
    

// Module headers:
#include "shortest-path-helper.h"
#include "shortest-path-table.h"
#include "shortest-path.h"
#endif
