#ifndef GPSRQ_PTABLE_H
#define GPSRQ_PTABLE_H

#include <map>
#include <cassert>
#include <stdint.h>
#include <complex> 
#include <sys/types.h> 

#include "ns3/ipv4.h"
#include "ns3/timer.h"
#include "ns3/node.h"
#include "ns3/node-list.h"
#include "ns3/mobility-model.h"
#include "ns3/vector.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/random-variable-stream.h"
#include "ns3/output-stream-wrapper.h"
#include "ns3/location-service.h"
#include "gpsrq-packet.h"

#include "ns3/qkd-manager.h"

namespace ns3 {
namespace gpsrq {

#define ABS(x) (((x)<0)?(0-(x)):(x));

struct CompareVectors
{
    bool operator()(const Vector& a, const Vector& b)
    {	
		// Note I use three separate if statements here for clarity.
		// Combining them into a single statement is trivial/
		//
		if ((a.z < b.z)                                        )	{return true;}
		if ((a.z == b.z) && (a.y < b.y)                    ) 		{return true;}
		if ((a.z == b.z) && (a.y == b.y) && (a.x < b.x)) 			{return true;}

		return false;
    }
};

/*
 * \ingroup gpsrq
 * \brief Position table used by GPSRQ
 */
struct PositionTableEntry{
	//Ipv4Address address; //address of the neighbour node
	std::pair<Vector, Time> position;	//position of the neighbour node
	uint32_t interface; //local interface which received the hello package from the neighbour
};
  
struct TempCacheEntry{
	Ipv4Address	src; //address of the neighbour node
	Ipv4Address	dst; //address of the neighbour node 
	uint32_t 	interface; //local interface which is used to forward the packet
};

struct FinalCacheEntry{
  uint32_t    interface;    //local interface which is used to forward the packet
  Vector      cacheCenter;  //coordinates of circle cache center
  double      cacheRadius;  //radius of circle cache
  Ipv4Address nextHop;      //address of the nextHop which can not be used for reaching of the cached circle 
  int64_t     timestamp;    //generation timestamp in nanoseconds
};

class PositionTable
{
public:
  /// c-tor
  PositionTable ();

  /**
   * \brief Gets the last time the entry was updated
   * \param id Ipv4Address to get time of update from
   * \return Time of last update to the position
   */
  Time GetEntryUpdateTime (Ipv4Address id);

  /**
   * \brief Adds entry in position table
   */
  void AddEntry (Ipv4Address id, Vector position, uint32_t interface);

  /**
   * \brief Deletes entry in position table
   */
  void DeleteEntry (Ipv4Address id);

  /**
   * \brief Gets position from position table
   * \param id Ipv4Address to get position from
   * \return Position of that id or NULL if not known
   */
  Vector GetPosition (Ipv4Address id, uint32_t &interface);

  /**
   * \brief Checks if a node is a neighbour
   * \param id Ipv4Address of the node to check
   * \return True if the node is neighbour, false otherwise
   */
  bool isNeighbour (Ipv4Address id, uint32_t &interface);

  /**
   * \brief remove entries with expired lifetime
   */
  void Purge ();

  /**
   * \brief clears all entries
   */
  void Clear ();

  /**
   * \Get Callback to ProcessTxError
   */
  Callback<void, WifiMacHeader const &> GetTxErrorCallback () const
  {
    return m_txErrorCallback;
  }

  /**GPSR
   * \brief Gets next hop according to GPSRQ protocol
   * \param position the position of the destination node
   * \param nodePos the position of the node that has the packet
   * \return Ipv4Address of the next hop, Ipv4Address::GetZero () if no nighbour was found in greedy mode
   */
  Ipv4Address BestNeighbor (
    Ptr<Packet> p,
    uint32_t tos,
    Ptr<Node> node,
    Ipv4Address src, 
    Ipv4Address dst, 
    Vector dstPosition,
    Vector myPosition, 
    uint32_t& outputInterface,
    bool cacheEnabled
  );

  bool IsInSearch (Ipv4Address id);

  Ipv4Address NextHopByInterface (uint32_t iif);

  bool HasPosition (Ipv4Address id);

  static Vector GetInvalidPosition ()
  {
    return Vector (-1, -1, 0);
  }

  /**
   * \brief Gets next hop according to GPSRQ recovery-mode protocol (right hand rule)
   * \param previousHop the position of the node that sent the packet to this node
   * \param nodePos the position of the destination node
   * \return Ipv4Address of the next hop, Ipv4Address::GetZero () if no nighbour was found in greedy mode
   */ 
  Ipv4Address BestAngle (
    Ptr<Packet> p, 
    Ipv4Address src, 
    Ipv4Address dst,  
    Ipv4Address previousHop, 
    Ipv4Address myAddress, 
    Ptr<Node> myNode, 
    uint32_t &outputInterface,  
    uint32_t inputInterface, 
    bool cacheEnabled, 
    bool &validPathExist
  );

  //Gives angle between the vector CentrePos-Refpos to the vector CentrePos-node counterclockwise
  double GetAngle (Vector centrePos, Vector refPos, Vector node);

  void Print (Ptr<OutputStreamWrapper> stream) const;

  //Temporary cache
  void AddNextHopToTempLoopTable (Ipv4Address src, Ipv4Address dst, uint32_t interface);

  bool CheckForLoopDetection (Ipv4Address src, Ipv4Address dst, uint32_t iif);

  void DeleteTempLoopEntry(Ipv4Address dst, uint32_t iif);

  //Final cache
  bool  CheckCachedValues(Ipv4Address dst, Ipv4Address nextHop);

  void AddNewCacheEntry (Ipv4Address nextHop, Ipv4Address dst, int64_t timestamp);

  //Location service
  Ptr<LocationService> GetLS ();

  void SetLS (Ptr<LocationService> locationService);

private:

  Ptr<LocationService> m_locationService;

	Time m_entryLifeTime;

	std::map<Ipv4Address, PositionTableEntry> m_table;

  //This cache table is used for speeding up the calculation, it is not used for loop detection!
	std::map<Vector, Ipv4Address, CompareVectors> m_cacheTable;

  //This cache table is used for detection of loops!
	std::map<std::string, TempCacheEntry> m_cacheForwardingTable;

  //This table is used for storage of currently unavailable nodes
  std::vector<FinalCacheEntry> m_finalCache;

	//std::map<Ipv4Address, std::pair<Vector, Time>, uint32_t> m_table;
	// TX error callback
	Callback<void, WifiMacHeader const &> m_txErrorCallback;
	// Process layer 2 TX error notification
	void ProcessTxError (WifiMacHeader const&);

  bool  inCircle( Vector myPos, Vector circlePos, double R );

};

}   // gpsrq
} // ns3
#endif /* GPSRQ_PTABLE_H */
