/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#ifndef QKD_INTERNAL_TAG_H
#define QKD_INTERNAL_TAG_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/tag.h"
#include "ns3/object.h"

namespace ns3 {


/**
*   QKDCommandTag is needed to forward IP address of nextHop to 
*   QKDManager:VirtualSend which is then in charge to forward packet to the
*   underlay network
*   This tag is added to packet in virtual-ipv4-l3-protocol::SendRealOut 
*    function and removed in QKDManager:VirutalSend function
*/
class QKDCommandTag : public Tag
{
public:
    static 	       TypeId GetTypeId (void);
    virtual 	     TypeId GetInstanceTypeId (void) const;
    virtual 	     uint32_t GetSerializedSize (void) const;
    virtual 	     void Serialize (TagBuffer i) const;
    virtual 	     void Deserialize (TagBuffer i);
    virtual 	     void Print (std::ostream &os) const;

    void           SetCommand (char value);
    char 	         GetCommand (void) const;

    void 		       SetRoutingProtocolNumber (uint32_t value);
    uint32_t 	     GetRoutingProtocolNumber (void) const;

private:
    char           m_command;
    uint32_t       m_routingProtocolNumber;
};

/**
*   QKDInternalNextHopTag is needed to forward IP address of nextHop to 
*   QKDManager:VirtualSend which is then in charge to forward packet to the
*   underlay network
*   This tag is added to packet in virtual-ipv4-l3-protocol::SendRealOut 
*    function and removed in QKDManager:VirutalSend function
*/
class QKDInternalNextHopTag : public Tag
{
public:
    static 	      TypeId GetTypeId (void);
    virtual 	    TypeId GetInstanceTypeId (void) const;
    virtual 	    uint32_t GetSerializedSize (void) const;
    virtual 	    void Serialize (TagBuffer i) const;
    virtual 	    void Deserialize (TagBuffer i);
    virtual 	    void Print (std::ostream &os) const;

    void 		      SetNextHopValue (Ipv4Address value);
    Ipv4Address 	GetNextHopValue (void) const;

    void 		      SetSourceAddress (Ipv4Address value);
    Ipv4Address 	GetSourceAddress (void) const;
    
    void          SetTos (uint8_t tos);
    uint8_t       GetTos (void) const;

private:
    uint8_t         m_tos;
    Ipv4Address     m_nextHop;  
    Ipv4Address     m_sourceAddress;  
};




// define this class in a public header
class QKDInternalTag : public Tag
{
public:
  static 	  TypeId GetTypeId (void);
  virtual 	TypeId GetInstanceTypeId (void) const;
  virtual 	uint32_t GetSerializedSize (void) const;
  virtual 	void Serialize (TagBuffer i) const;
  virtual 	void Deserialize (TagBuffer i);
  virtual 	void Print (std::ostream &os) const;

  // these are our accessors to our tag structure
  void 		 SetEncryptValue (uint8_t value);
  uint8_t  GetEncryptValue (void) const;

  void 		 SetAuthenticateValue (uint8_t value);
  uint8_t  GetAuthenticateValue (void) const;

  void     SetMaxDelayValue (uint32_t value);
  uint32_t GetMaxDelayValue (void) const;

private:
  uint8_t 	m_encryptValue;
  uint8_t 	m_authenticateValue;
  uint32_t  m_maxDelay;
};
}  
// namespace ns3

#endif /* QKD_INTERNAL_TAG_H */


