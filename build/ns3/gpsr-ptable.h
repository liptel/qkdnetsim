#ifndef GPSR_PTABLE_H
#define GPSR_PTABLE_H

#include <map>
#include <cassert>
#include <stdint.h>
#include <complex> 
#include <sys/types.h> 

#include "ns3/ipv4.h"
#include "ns3/timer.h"
#include "ns3/node.h"
#include "ns3/node-list.h"
#include "ns3/mobility-model.h"
#include "ns3/vector.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/random-variable-stream.h"
#include "ns3/output-stream-wrapper.h"
#include "gpsr-packet.h"

namespace ns3 {
namespace gpsr {

struct CompareVectors
{
    bool operator()(const Vector& a, const Vector& b)
    {	
		// Note I use three separate if statements here for clarity.
		// Combining them into a single statement is trivial/
		//
		if ((a.z < b.z)                                        )	{return true;}
		if ((a.z == b.z) && (a.y < b.y)                    ) 		{return true;}
		if ((a.z == b.z) && (a.y == b.y) && (a.x < b.x)) 			{return true;}

		return false;
    }
};



/*
 * \ingroup gpsr
 * \brief Position table used by GPSR
 */
struct PositionTableEntry{
	//Ipv4Address address; //address of the neighbour node
	std::pair<Vector, Time> position;	//position of the neighbour node
	uint32_t interface; //local interface which received the hello package from the neighbour
};
  
struct CachedOutputEntry{
	Ipv4Address	src; //address of the neighbour node
	Ipv4Address	dst; //address of the neighbour node 
	uint32_t 	interface; //local interface which is used to forward the packet
};

class PositionTable
{
public:
  /// c-tor
  PositionTable ();

  /**
   * \brief Gets the last time the entry was updated
   * \param id Ipv4Address to get time of update from
   * \return Time of last update to the position
   */
  Time GetEntryUpdateTime (Ipv4Address id);

  /**
   * \brief Adds entry in position table
   */
  void AddEntry (Ipv4Address id, Vector position, uint32_t interface);

  /**
   * \brief Deletes entry in position table
   */
  void DeleteEntry (Ipv4Address id);

  /**
   * \brief Gets position from position table
   * \param id Ipv4Address to get position from
   * \return Position of that id or NULL if not known
   */
  Vector GetPosition (Ipv4Address id, uint32_t &interface);

  /**
   * \brief Checks if a node is a neighbour
   * \param id Ipv4Address of the node to check
   * \return True if the node is neighbour, false otherwise
   */
  bool isNeighbour (Ipv4Address id, uint32_t &interface);

  /**
   * \brief remove entries with expired lifetime
   */
  void Purge ();

  /**
   * \brief clears all entries
   */
  void Clear ();

  /**
   * \Get Callback to ProcessTxError
   */
  Callback<void, WifiMacHeader const &> GetTxErrorCallback () const
  {
    return m_txErrorCallback;
  }

  /**
   * \brief Gets next hop according to GPSR protocol
   * \param position the position of the destination node
   * \param nodePos the position of the node that has the packet
   * \return Ipv4Address of the next hop, Ipv4Address::GetZero () if no nighbour was found in greedy mode
   */
  Ipv4Address BestNeighbor (Ipv4Address src, Ipv4Address dst,  Vector position, Vector nodePos, uint32_t &outputInterface, bool cacheEnabled, uint32_t inputInterface);

  bool IsInSearch (Ipv4Address id);

  bool HasPosition (Ipv4Address id);

  static Vector GetInvalidPosition ()
  {
    return Vector (-1, -1, 0);
  }

  /**
   * \brief Gets next hop according to GPSR recovery-mode protocol (right hand rule)
   * \param previousHop the position of the node that sent the packet to this node
   * \param nodePos the position of the destination node
   * \return Ipv4Address of the next hop, Ipv4Address::GetZero () if no nighbour was found in greedy mode
   */
  Ipv4Address BestAngle (Ipv4Address src, Ipv4Address dst, Vector previousHop, Vector nodePos, uint32_t &interface, bool cacheEnabled, uint32_t inputInterface);

  //Gives angle between the vector CentrePos-Refpos to the vector CentrePos-node counterclockwise
  double GetAngle (Vector centrePos, Vector refPos, Vector node);

  void Print (Ptr<OutputStreamWrapper> stream) const;

  void AddNextHopToCache(Ipv4Address src, Ipv4Address dst, uint32_t interface);

private:
	Time m_entryLifeTime;
	std::map<Ipv4Address, PositionTableEntry> m_table;
	std::map<Vector, Ipv4Address, CompareVectors> m_cacheTable;
	std::map<std::string, CachedOutputEntry> m_cacheForwardingTable;

	//std::map<Ipv4Address, std::pair<Vector, Time>, uint32_t> m_table;
	// TX error callback
	Callback<void, WifiMacHeader const &> m_txErrorCallback;
	// Process layer 2 TX error notification
	void ProcessTxError (WifiMacHeader const&);
};

}   // gpsr
} // ns3
#endif /* GPSR_PTABLE_H */
