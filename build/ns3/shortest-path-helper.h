/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef SHORTEST_PATH_HELPER_H
#define SHORTEST_PATH_HELPER_H

#include "ns3/object-factory.h"
#include "ns3/node.h"
#include "ns3/node-container.h"
#include "ns3/ipv4-routing-helper.h"
#include "ns3/shortest-path.h"

namespace ns3 {


class ShortestPathHelper : public Ipv4RoutingHelper
{
public:
  ShortestPathHelper();

  ShortestPathHelper* Copy (void) const;

  virtual Ptr<Ipv4RoutingProtocol> Create (Ptr<Node> node) const;

  void Set (std::string name, const AttributeValue &value);
  
private:
  ObjectFactory m_agentFactory;
};


}

#endif /* SHORTEST_PATH_HELPER_H */

