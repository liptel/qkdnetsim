/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */



#include <iostream>
#include "ns3/header.h"
#include "ns3/enum.h"
#include "ns3/ipv4-address.h"
#include <map>
#include "ns3/nstime.h"
#include "ns3/vector.h"
#include "ns3/tag.h"
//#include "gpsrq-status-header.h"

#ifndef GPSRQPACKET_H
#define GPSRQPACKET_H

#define GPSRQ_TYPE_POSITION_HEADER_PROTOCOL_NUMBER 145
#define GPSRQ_TYPE_HELLO_HEADER_PROTOCOL_NUMBER 146
#define GPSRQ_POSITION_HEADER_PROTOCOL_NUMBER 147
#define GPSRQ_HELLO_HEADER_PROTOCOL_NUMBER 148

namespace ns3 {
namespace gpsrq {
   
enum MessageType
{
  GPSRQTYPE_HELLO  = 1,         //!< GPSRQTYPE_HELLO
  GPSRQTYPE_POS = 2,            //!< GPSRQTYPE_POS
};

/* DEPRECATED
class GPSRQInternalTag : public Tag
{
public:
  static 	TypeId GetTypeId (void);
  virtual 	TypeId GetInstanceTypeId (void) const;
  virtual 	uint32_t GetSerializedSize (void) const;
  virtual 	void Serialize (TagBuffer i) const;
  virtual 	void Deserialize (TagBuffer i);
  virtual 	void Print (std::ostream &os) const;
  
  void 		SetProtocol (uint8_t value);
  uint8_t 	GetProtocol (void) const;

private: 
  uint8_t 	m_protocol; 
};
*/

/**
 * \ingroup gpsrq
 * \brief GPSRQ types
 */
class TypeHeader : public Header
{
public:
  /// c-tor
  TypeHeader (MessageType t);

  ///\name Header serialization/deserialization
  //\{
  static TypeId GetTypeId ();
  TypeId GetInstanceTypeId () const;
  uint32_t GetSerializedSize () const;
  void Serialize (Buffer::Iterator start) const;
  uint32_t Deserialize (Buffer::Iterator start);
  void Print (std::ostream &os) const;
  //\}

  /// Return type
  MessageType Get () const
  {
    return m_type;
  }
  /// Check that type if valid
  bool IsValid () const
  {
    return m_valid; //FIXME that way it wont work
  }
  bool operator== (TypeHeader const & o) const;
private:
  MessageType m_type;
  bool m_valid;
};
std::ostream & operator<< (std::ostream & os, TypeHeader const & h);



class HelloHeader : public Header
{
public:
  /// c-tor
  HelloHeader (uint64_t originPosx = 0, uint64_t originPosy = 0);

  ///\name Header serialization/deserialization
  //\{
  static TypeId GetTypeId ();
  TypeId GetInstanceTypeId () const;
  uint32_t GetSerializedSize () const;
  void Serialize (Buffer::Iterator start) const;
  uint32_t Deserialize (Buffer::Iterator start);
  void Print (std::ostream &os) const;  

  //\}

  ///\name Fields
  //\{
  void SetOriginPosx (uint64_t posx)
  {
    m_originPosx = posx;
  }
  uint64_t GetOriginPosx () const
  {
    return m_originPosx;
  }
  void SetOriginPosy (uint64_t posy)
  {
    m_originPosy = posy;
  }
  uint64_t GetOriginPosy () const
  {
    return m_originPosy;
  }
  //\} 

  bool operator== (HelloHeader const & o) const;
private:
  uint64_t         m_originPosx;          ///< Originator Position x
  uint64_t         m_originPosy;          ///< Originator Position x 
};
std::ostream & operator<< (std::ostream & os, HelloHeader const &);


class PositionHeader : public Header
{
public:

  PositionHeader ();

  PositionHeader (
    uint32_t updated,
    Ipv4Address rec,
    uint8_t inRec,  
    Ipv4Address last
  );
 
  static TypeId GetTypeId ();
  TypeId GetInstanceTypeId () const;
  uint32_t GetSerializedSize () const;
  void Serialize (Buffer::Iterator start) const;
  uint32_t Deserialize (Buffer::Iterator start);
  void Print (std::ostream &os) const;

  void SetProtocol (uint8_t num);
  uint8_t GetProtocol () const; 

  void SetUpdated (uint32_t updated)
  {
    m_updated = updated;
  }
  uint32_t GetUpdated () const
  {
    return m_updated;
  }

  void SetInRec (uint8_t rec)
  {
    m_inRec = rec;
  }
  uint8_t GetInRec () const
  {
    return m_inRec;
  }
  /*
  void SetDst (Ipv4Address posx)
  {
    m_dst = posx;
  }
  Ipv4Address GetDst () const
  {
    return m_dst;
  }
  */
  void SetRec (Ipv4Address posx)
  {
    m_rec = posx;
  }
  Ipv4Address GetRec () const
  {
    return m_rec;
  }
  
  void SetLast (Ipv4Address posx)
  {
    m_last = posx;
  }
  Ipv4Address GetLast () const
  {
    return m_last;
  }

  void    SetReturningLoop (uint8_t value)
  {
      m_returningLoopDetected = value;
  }
  uint8_t   GetReturningLoop (void) const
  {
    return m_returningLoopDetected;
  }
        
 
  bool operator== (PositionHeader const & o) const;
private:
  uint32_t         m_updated;           ///< Time of last update
  Ipv4Address      m_rec;               ///< Address of node where entered Recovery-mode 
  uint8_t          m_inRec;             ///< 1 if in Recovery-mode, 0 otherwise 
  Ipv4Address      m_last;              ///< Address of previous hop 
  uint32_t         m_protocol;          ///!< Protocol 
  uint8_t          m_returningLoopDetected;
};

std::ostream & operator<< (std::ostream & os, PositionHeader const &);

 


}
}
#endif /* GPSRQPACKET_H */
