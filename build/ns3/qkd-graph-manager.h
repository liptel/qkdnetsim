/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#ifndef QKD_GRAPH_MANAGER_H
#define QKD_GRAPH_MANAGER_H

#include <fstream>
#include <sstream>
#include <vector>
#include "ns3/object.h"
#include "ns3/gnuplot.h"
#include "ns3/core-module.h"
#include "ns3/node-list.h" 
#include "qkd-graph.h" 
#include "qkd-total-graph.h" 

namespace ns3 {

class QKDGraph;

class QKDGraphManager : public Object 
{
public:
   
    /**
    * \brief Get the type ID.
    * \return the object TypeId
    */
    static TypeId GetTypeId (void);

	virtual ~QKDGraphManager(); 

	static QKDGraphManager* getInstance();

	void 	PrintGraphs(); 
  
	void AddBuffer(uint32_t nodeID, uint32_t bufferID, std::string graphName, std::string graphType);

	static void ProcessCurrentChange(std::string context, uint32_t value);

	static void ProcessStatusChange(std::string context, uint32_t value);

	static void ProcessThresholdChange(std::string context, uint32_t value);

	// FOR QKD TOTAL GRAPH

	static void ProcessCurrentIncrease(std::string context, uint32_t value);

	static void ProcessCurrentDecrease(std::string context, uint32_t value);

	static void ProcessThresholdIncrease(std::string context, uint32_t value);

	static void ProcessThresholdDecrease(std::string context, uint32_t value);
	
	void SendStatusValueToGraph(const uint32_t& nodeID, const uint32_t& bufferID, const uint32_t& value);

	void SendCurrentChangeValueToGraph(const uint32_t& nodeID, const uint32_t& bufferID, const uint32_t& value);

	void SendThresholdValueToGraph(const uint32_t& nodeID, const uint32_t& bufferID, const uint32_t& value);

	Ptr<QKDTotalGraph> GetTotalGraph(); 
	
private:  

	QKDGraphManager (){};

	static bool instanceFlag;

	static QKDGraphManager *single;

	static Ptr<QKDTotalGraph> m_totalGraph;
	
	std::vector<std::vector<QKDGraph *> > m_graphs; //vector of nodes-device-channels 
};
}
#endif /* QKD_GRAPH_MANAGER */
