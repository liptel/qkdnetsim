/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 LIPTEL.ieee.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Miralem Mehic <miralem.mehic@ieee.org>
 */

#ifndef QKD_BUFFER_H
#define QKD_BUFFER_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/object.h"
#include "ns3/ipv4-header.h" 
#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/event-id.h"
#include "ns3/qkd-key.h"
#include <vector>

#include <crypto++/iterhash.h>
#include <crypto++/secblock.h>
 
namespace ns3 {


/**
 * \ingroup QKD
 * \brief Simple QKD Manager that controls the QKD storage and QKD link status.
 *
 * \see Attach
 * \see TransmitStart
 */
class QKDBuffer : public Object
{
public:
    
    static const uint32_t  QKDSTATUS_READY        = 0;
    static const uint32_t  QKDSTATUS_WARNING      = 1;
    static const uint32_t  QKDSTATUS_CHARGING     = 2;
    static const uint32_t  QKDSTATUS_EMPTY        = 3;
    
    struct data{
        uint32_t value;
        uint32_t position;
    };
    
    /**
    * \brief Get the TypeId
    *
    * \return The TypeId for this class
    */
    static TypeId GetTypeId (void);

    /**
    * \brief Create a QKDBuffer
    *
    * By default, you get an empty QKD storage
    */
    QKDBuffer (uint32_t srcNodeId, uint32_t dstNodeId, bool useRealStorages);

    virtual ~QKDBuffer ();
  
    void Init(void);

    void Dispose(void);
    
    /**
    * \brief Check is there enough key material to send the package to destination
    *
    * \return Ptr<QKDKey> if there is enough key material; 0 otherwise
    */
    Ptr<QKDKey> ProcessOutgoingRequest(const uint32_t& keySize);

    /**
    * \brief Check is there enough key material to decrypt incoming the package
    *
    * \return Ptr<QKDKey> if there is enough key material; 0 otherwise
    */
    Ptr<QKDKey> ProcessIncomingRequest(const uint32_t& keyID, const uint32_t& keySize);
    
    bool AddNewContent(const uint32_t& keySize);

    uint32_t FetchState(void);

    uint32_t FetchPreviousState(void);
  
    void CalculateAverageAmountOfTheKeyInTheBuffer();
   
    void CheckState(void);

    void KeyCalculation();

    int64_t FetchDeltaTime();

    int64_t FetchLastKeyChargingTimeDuration();

    double FetchAverageKeyChargingTimePeriod();

    //virtual bool SetMcurrent (const uint32_t data);
    uint32_t GetMCurrentPrevious (void) const; 

    uint32_t GetMcurrent (void) const;

    uint32_t GetMthr (void) const;

    void SetMthr (uint32_t thr);

    uint32_t GetMmax (void) const;

    uint32_t GetMmin (void) const; 

    void    InitTotalGraph() const;

    bool    operator== (QKDBuffer const & o) const;


    uint32_t            m_SrcNodeId;

    uint32_t            m_DstNodeId;

    uint32_t            m_bufferID;

    static uint32_t     nBuffers;

    std::string         m_reassemblyCache; //Reassambly cache of TCP connection

private:       

    bool        m_useRealStorages;

    uint32_t    m_nextKeyID;

	std::map<uint32_t, Ptr<QKDKey> > m_keys;

    uint32_t    m_noEntry;
    
    uint32_t    m_period; 

    uint32_t    m_noAddNewValue;

    uint32_t    m_bitsChargedInTimePeriod;
    
    uint32_t    m_bitsUsedInTimePeriod;

    uint32_t    m_recalculateTimePeriod;

    double      m_c; //average amount of key in the buffer during the recalculate time period

    std::vector<struct QKDBuffer::data> m_previousValues;
  
    bool        m_isRisingCurve;
    
    uint32_t    m_localMax;

    uint32_t    m_localMaxNoEntry;

    uint32_t    m_previousStatus;
    /**
    * The minimal amount of key material in QKD key storage
    */
    uint32_t       m_Mmin;

    /**
    * The maximal amount of key material in QKD key storage
    */
    uint32_t       m_Mmax;

    /**
    * The threshold amount of key material in QKD key storage
    */
    uint32_t       m_Mthr; 

    TracedCallback<uint32_t > m_MthrChangeTrace;
    TracedCallback<uint32_t > m_MthrIncreaseTrace;
    TracedCallback<uint32_t > m_MthrDecreaseTrace;

    /**
    * The current amount of key material in QKD key storage
    */
    uint32_t       m_Mcurrent;

    uint32_t       m_McurrentPrevious;

    Ptr<QKDKey>     FetchKeyByID (const uint32_t& keyID);

    Ptr<QKDKey>     FetchKeyOfSize (const uint32_t& keySize);

    int64_t         m_lastKeyChargingTimeStamp;
    int64_t         m_lastKeyChargingTimeDuration;
    uint32_t        m_maxNumberOfRecordedKeyChargingTimePeriods;

    std::vector<int64_t > m_chargingTimePeriods;


    /**
    * The state of the Net Device transmit state machine.
    */
    uint32_t        m_Status;
    double          m_AverageKeyChargingTimePeriod;

    TracedCallback<uint32_t > m_McurrentChangeTrace;
    TracedCallback<uint32_t > m_McurrentIncreaseTrace;
    TracedCallback<uint32_t > m_McurrentDecreaseTrace;



    TracedCallback<uint32_t > m_StatusChangeTrace;   
    TracedCallback<double   > m_CMetricChangeTrace;   
    TracedCallback<double  > m_AverageKeyChargingTimePeriodTrace;    

    EventId         m_calculateRoutingMetric;

};
}

#endif /* QKD_BUFFER_H */
