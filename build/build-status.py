#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.26-secoqc_p2p_qkd_system-debug', 'build/scratch/ns3.26-simple_net_p2p_system-debug', 'build/scratch/ns3.26-secoqc_3_p2p_qkd_system-debug', 'build/scratch/ns3.26-qkd_overlay_channel_test-debug', 'build/scratch/ns3.26-qkd_channel_test-debug', 'build/scratch/ns3.26-qkd_overlay_secoqc_test-debug', 'build/scratch/ns3.26-secoqc_3_p2p_system-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

