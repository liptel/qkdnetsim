Quantum Key Distribution Network Simulation Module in the Network Simulator NS-3 (v3.26)

Project webpage: http://www.qkdnetsim.info/ 

 --------------------------------

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This repository is deprecated! 
Please use latest QKDNetSim code available at https://github.com/QKDNetSim/qkdnetsim-dev

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



As research in Quantum Key Distribution (QKD) technology 
grows larger and more complex, the need for highly accurate 
and scalable simulation technologies becomes important to 
assess the practical feasibility and foresee difficulties 
in the practical implementation of theoretical achievements. 
Due to the specificity of QKD link which requires optical and 
Internet connection between the network nodes, it is very costly 
to deploy a complete testbed containing multiple network hosts 
and links to validate and verify a certain network algorithm or 
protocol. The network simulators in these circumstances save a 
lot of money and time in accomplishing such task. A simulation 
environment offers the creation of complex network topologies, 
a high degree of control and repeatable experiments, which in turn 
allows researchers to conduct exactly the same experiments and 
confirm their results.  

The aim of Quantum Key Distribution Network Simulation Module (QKDNetSim)
 project was not to develop the entire simulator from scratch but
to develop the QKD simulation module in some of the already 
existing well-proven simulators. 
QKDNetSim is intended to facilitate additional understanding of 
QKD technology with respect to the existing network solutions. 
It seeks to serve as the natural playground for taking the further 
steps into this research direction (even towards practical 
exploitation in subsequent projects or product design).

Here, we provide the QKDNetSim source code which was developed 
in the network simulator of version 3 (NS-3). 
The module supports simulation of QKD network in overlay mode or 
in a single TCP/IP mode. Therefore, it can be used for 
simulation of other network technologies regardless of QKD.
 


Table of Contents:
------------------

1) An overview
2) Building ns-3
3) Running ns-3
4) Getting access to the ns-3 documentation 

Note:  Much more substantial information about ns-3 can be found at
http://www.nsnam.org

1) An Open Source project
-------------------------

ns-3 is a free open source project aiming to build a discrete-event
network simulator targeted for simulation research and education.   
This is a collaborative project; we hope that
the missing pieces of the models we have not yet implemented
will be contributed by the community in an open collaboration
process.

Contributing to the ns-3 project is still a very informal
process because that process depends heavily on the background
of the people involved, the amount of time they can invest
and the type of model they want to work on.  

Despite this lack of a formal process, there are a number of 
steps which naturally stem from the open-source roots of the
project.  These steps are described in doc/contributing.txt

2) Building ns-3
----------------

On the beginning, we need to install prerequisites as root:
**sudo apt-get install gcc g++ python python-dev mercurial bzr 
gdb valgrind gsl-bin libgsl2 flex bison tcpdump 
sqlite sqlite3 libsqlite3-dev libxml2 libxml2-dev libgtk2.0-0 
libgtk2.0-dev uncrustify doxygen graphviz imagemagick texlive 
texlive-latex-extra texlive-generic-extra texlive-generic-recommended 
texinfo dia texlive texlive-latex-extra texlive-extra-utils 
texlive-generic-recommended texi2html python-pygraphviz 
python-kiwi python-pygoocanvas libgoocanvas-dev python-pygccxml git libcrypto++-dev libcrypto++-doc libcrypto++-utils libboost-dev libboost-all-dev**


The code for the framework and the default models provided
by ns-3 is built as a set of libraries. User simulations
are expected to be written as simple programs that make
use of these ns-3 libraries.

To build the set of default libraries and the example
programs included in this package, you need to use the
tool 'waf'. Detailed information on how use waf is 
included in the file doc/build.txt

However, the real quick and dirty way to get started is to
type the command 

**./waf configure; ./waf** 

the the directory which contains
this README file. The files built will be copied in the
build/debug or build/optimized.

The current codebase is expected to build and run on the
set of platforms listed in the RELEASE_NOTES file.

Other platforms may or may not work: we welcome patches to 
improve the portability of the code to these other platforms. 

3) Running ns-3
---------------

On recent Linux systems, once you have built ns-3, it 
should be easy to run the sample programs with the
following command:

**./waf --run scratch/secoqc_3_p2p_qkd_system**

That program should generate a simple-global-routing.tr text 
trace file and a set of simple-global-routing-xx-xx.pcap binary
pcap trace files, which can be read by tcpdump -tt -r filename.pcap

4) Getting access to the ns-3 documentation
-------------------------------------------

Once you have verified that your build of ns-3 works by running
the simple-point-to-point example as outlined in 4) above, it is
quite likely that you will want to get started on reading
some ns-3 documentation. 

All of that documentation should always be available from
the ns-3 website: http:://www.nsnam.org/ but we
include some of it in this release for ease of use.

This documentation includes:

  - a tutorial
 
  - a manual

  - a wiki for user-contributed tips: http://www.nsnam.org/wiki/

  - an API documentation generated using doxygen: this is
    a reference manual, most likely not very well suited 
    as introductory text:
    http://www.nsnam.org/doxygen/index.html
 

*Author: Miralem Mehic <miralem.mehic@ieee.org>*
